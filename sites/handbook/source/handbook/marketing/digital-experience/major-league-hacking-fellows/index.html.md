---
layout: handbook-page-toc
title: "Major League Hacking Fellows"
description: "Information on the MLHF cohorts working with Digital Experience."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Program Information
{:.no_toc}

The purpose of this page is to present process guidelines for the Major League Hacking Fellow cohorts (https://fellowship.mlh.io/) that are working with the Digital Experience team. This page is for fellows to find information about their cohort, the Digital Experience team, our projects, and where to ask for help. 

## Fall 2022 Cohort 

The Fall 20022 Cohort working with GitLab will be comprised of 2 engineering fellows. The will be making open source contributions to the Slippers Design System. 

- 9/19/22 - start - National Butterscotch Pudding Day
- 1st week - orientation, team building, onboarding 
- 12/16/22 - end date (12 weeks ) 

### Improving the Slipper Design System at GitLab

#### Summary of the Project

At GitLab, the Slippers Design System is a systematic approach to marketing site development  You can use the system to manage our marketing component library.

In this project, the MLH fellows will complete several deliverables that will improve the user experience of the Slippers Design System. We will implement new components, iterate on existing components, and use UI components from GitLab’s Pajamas design system to bring consistency across the GitLab application and marketing site.
  
#### Stakeholder Information

Please list all stakeholders (name, email, title/role) who will be involved with the project.

| Name      | Role | GitLab Handle |
| ----------- | ----------- | ----------- |
| Lauren Barker| maintainer | @laurenbarker |
| Megan Filo | maintainer | @meganfilo |
| [Gideon Tong](https://www.linkedin.com/in/gideontong/)| MLHF Pod Leader |  |
| [Chukwuemeka Mba](http://linkedin.com/in/emekamba)| MLHF Fellow | |
| [Uy Seng](http://linkedin.com/in/uy-seng-704843196)| MLHF Fellow | |
| [Seung Yoo](http://linkedin.com/in/seungmin-yoo-01376932)| MLHF Fellow | |

#### Fellow Technical Onboarding

Slippers is the open-source GitLab Marketing Web Design System. It was created in the spirit of "[everyone can contribute](https://about.gitlab.com/company/mission/#mission)". Guidelines enable people to build better web pages and co-create with ease. 

To get started with contributing to the Slippers Design System, create a [free SaaS account](https://about.gitlab.com/pricing/) with GitLab. 

[Watch this video](https://youtu.be/dphm0TlAqIk) on how it's currently built, published, and used on the marketing site as of August 2022.

Create a fork of the [Slippers repository](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui). 

Follow the README instructions for setting your local development environment.

#### Tutorials/Videos

-   Create a GitLab account and [set up SSH keys](https://docs.gitlab.com/ee/user/ssh.html).
    
-   [https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md)
    
-   If you don’t know VueJS 2.0, you can get started on this [guide](https://v2.vuejs.org/v2/guide/)
    
-   [Slippers Design System walkthough video](https://youtu.be/dphm0TlAqIk)

#### Documentation

-   [Read GitLab’s contribution guidelines](https://docs.gitlab.com/ee/development/contributing/index.html) with a special focus on the [Merge Request Workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html)
    
-   Get familiarized with GitLab’s Slipper Design System by reading the [intro and exploring our Storybook instance](https://gitlab-com.gitlab.io/marketing/digital-experience/slippers-ui/?path=/story/intro--page).   

#### Deliverables + Timeline

Please identify and describe the deliverables for your project(s) and the ideal deadline for each. This may include features or issues you would like the fellows to complete by the end of the fellowship. There are two general approaches we’ve seen be successful:

| **Issue**      | **Description** | **Assignee** | **Deadline** |
| ----------- | ----------- | ----------- | ----------- |
| [Vertically align dropdown caret](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/issues/194)| Vertically align dropdown caret |  TBD | October, 17th |
| [Loading components](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/issues/204)| Vertically align dropdown caret |  TBD | October, 17th |
| [Create tooltip component](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/issues/205)| Create a accordion component in the design system |  TBD | November, 17th |
| [Loading components](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/issues/204)| Vertically align dropdown caret |  TBD | October, 17th |
| [ENG: Accessible and compliant dropdowns](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/issues/132)| Iterate on dropdown component to. be accessible |  TBD | November, 17th |
| [Implement the Storybook/Figma plugin](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/issues/168)| Implement the Storybook/Figma plugin and make it available for use in the project |  TBD | November, 17th |
| More issues if there's capacity from fellows|  TBD | TBD |  December, 17th |

## MLH Partner Success Contact

-   [Matt Dillabough](mailto:matt.dillabough@majorleaguehacking.com)
    
---
